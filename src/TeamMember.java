/**
 * Created by j on 28/03/17.
 */
public class TeamMember {
    public static final String  GROUP_NAME = "PixelPushers";

    public String name = "";
    public String role = "";
    public boolean gitCommitted = false;


    public void printDetails() {

        if (name.length() != 0 && role.length() !=0) {

            System.out.println("Hello World, my name is " + name + ". " +
                    "I am a member of " + GROUP_NAME +
                    " and my role in the team is as a " + role + ".");
            this.gitCommitted= true;
        }
    }

    public boolean hasGitCommitted(){
        return this.gitCommitted;
    }
}