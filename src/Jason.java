/**
 * Created by j on 28/03/17.
 */
public class Jason extends TeamMember {

    //added this after deleting project locally and then cloning back to my pc from
    // https://S3493432@bitbucket.org/pixel-pushers/bitbucket-tutorial.git
    // Installed on Windows PC too now
    public String myName = "Jason";
    public String myRole = "Project Coordinator";

    public Jason() {
        this.name = myName;
        this.role = myRole;
    }
}

