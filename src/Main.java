import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {


        int commitCount = 0;

        ArrayList<TeamMember> teamMembers = new ArrayList<TeamMember>();
        teamMembers.add(new Jason());
        teamMembers.add(new Sam());
        teamMembers.add(new Sefton());
        teamMembers.add(new Ziggy());
        teamMembers.add(new Nic());

        System.out.println();

        for (TeamMember member : teamMembers)  {
            member.printDetails();
            if (member.hasGitCommitted()) {
                commitCount++;
            }
        }

        System.out.println();

        if (commitCount==5){
            System.out.println("Congratulations..... All team members have added, committed and pushed changes to the "+
                    "bitbucket repository.");
        }
        else if (commitCount==1){
            System.out.println("Only " +commitCount+ " team member has added, committed and pushed changes to the " +
                    "bitbucket repository.");
        }

        else {
            System.out.println("Only " +commitCount+ " team members have added, committed and pushed changes to the " +
                    "bitbucket repository.");
        }





    }
}
